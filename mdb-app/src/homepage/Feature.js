import React from 'react';
import { MDBCard, MDBCardTitle, MDBBtn, MDBRow, MDBCol, MDBIcon } from 'mdbreact';

const Feature = () => {
  return (
    <MDBRow className="justify-content-center m-5">
        <MDBCol md='12' lg='12' sm='12' className="m-5">
            <h2>Letest Feature</h2>
        </MDBCol>
      <MDBCol md='3' lg='3' sm='12'>
        <MDBCard
          className='card-image'
          style={{
            backgroundImage:
              "url('./shop_01.jpg')"
          }}
        >
          <div className='text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4'>
            <div>
              <h5 className='white-text fa-4x'>
                <MDBIcon icon='shipping-fast' />
              </h5>
              <MDBCardTitle tag='h3' className='pt-2'>
                <strong>Free Shipping</strong>
              </MDBCardTitle>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </p>

            </div>
          </div>
        </MDBCard>
      </MDBCol>

      <MDBCol md='3'>
        <MDBCard
          className='card-image'
          style={{
            backgroundImage:
              "url('./shop_01.jpg')"
          }}
        >
          <div className='text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4'>
            <div>
              <h5 className='white-text fa-4x'>
                <MDBIcon icon='headphones-alt' />
              </h5>
              <MDBCardTitle tag='h3' className='pt-2'>
                <strong>24X7 Support</strong>
              </MDBCardTitle>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </p>

            </div>
          </div>
        </MDBCard>
      </MDBCol>
<MDBCol md='3'>
        <MDBCard
          className='card-image'
          style={{
            backgroundImage:
              "url('./shop_01.jpg')"
          }}
        >
          <div className='text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4'>
            <div>
              <h5 className='white-text fa-4x'>
                <MDBIcon icon='award' />
              </h5>
              <MDBCardTitle tag='h3' className='pt-2'>
                <strong>Best Price</strong>
              </MDBCardTitle>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </p>

            </div>
          </div>
        </MDBCard>
      </MDBCol>
      <MDBCol md='3'>
        <MDBCard
          className='card-image'
          style={{
            backgroundImage:
              "url('./shop_01.jpg')"
          }}
        >
          <div className='text-white text-center d-flex align-items-center rgba-black-strong py-5 px-4'>
            <div>
              <h5 className='white-text fa-4x'>
                <MDBIcon icon='shopping-cart' />
              </h5>
              <MDBCardTitle tag='h3' className='pt-2'>
                <strong>Shopping Cart</strong>
              </MDBCardTitle>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </p>

            </div>
          </div>
        </MDBCard>
      </MDBCol>

    </MDBRow>
  )
}

export default Feature;