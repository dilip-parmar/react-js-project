import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

const FooterPage = () => {
  return (
    <MDBFooter color="dark" className="bg-dark font-small pt-4 mt-4">
      <MDBContainer fluid className="text-center text-md-left">
        <MDBRow>

          <MDBCol md="6" className="pl-5">
            <strong className="white-text"><img src="./logo.png" alt="logo" className="img-fluid"/></strong>
            <p>Here you can use rows and columns here to organize your footer content.Here you can use rows and columns here to organize your footer content.</p>
           </MDBCol>
          <MDBCol md="6" className="text-right pr-5">
            <h5 className="title">Important Links</h5>
            <ul>
              <li className="list-unstyled">
                <a href="#!">Shop</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">Accessories</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">Products</a>
              </li>
              <li className="list-unstyled">
                <a href="#!">About</a>
              </li>
            </ul>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: <a href="https://www.mdbootstrap.com"> 73lines.com </a>
        </MDBContainer>
      </div>
    </MDBFooter>
  );
}

export default FooterPage;