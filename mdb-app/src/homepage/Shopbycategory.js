import React from "react";
import { MDBCard, MDBCardTitle, MDBBtn, MDBCardGroup, MDBCardImage, MDBCardText, MDBCardBody } from "mdbreact";

const Shopbycategory = () => {
  return (
    <MDBCardGroup className="m-5">

      <MDBCard className="ml-4">
        <MDBCardImage src="./shop_01.jpg" alt="MDBCard image cap" top hover
          overlay="white-slight" />
        <MDBCardBody>
          <MDBCardTitle tag="h5">Shoes</MDBCardTitle>
          <MDBCardText>
            1 Product
          </MDBCardText>

        </MDBCardBody>
      </MDBCard>
       <MDBCard className="ml-4 ">
        <MDBCardImage src="./shop_02.jpg" alt="MDBCard image cap" top hover
          overlay="white-slight" />
        <MDBCardBody>
          <MDBCardTitle tag="h5">Clothing</MDBCardTitle>
          <MDBCardText>
            10 Product
          </MDBCardText>
        </MDBCardBody>
      </MDBCard>

     <MDBCard className="ml-4">
        <MDBCardImage src="./shop_03.jpg" alt="MDBCard image cap" top hover
          overlay="white-slight" />
         <MDBCardBody>
          <MDBCardTitle tag="h5">Accessories</MDBCardTitle>
          <MDBCardText>
           6 Product
          </MDBCardText>
        </MDBCardBody>
      </MDBCard>
      <MDBCard className="ml-4">
        <MDBCardImage src="./shop_04.jpg" alt="MDBCard image cap" top hover
          overlay="white-slight" />
        <MDBCardBody>
          <MDBCardTitle tag="h5">Gedgets</MDBCardTitle>
          <MDBCardText>
            6 Product
          </MDBCardText>

        </MDBCardBody>
      </MDBCard>
    </MDBCardGroup>
  );
}

export default Shopbycategory;