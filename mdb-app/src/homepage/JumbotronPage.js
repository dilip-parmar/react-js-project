import React from "react";
import { MDBJumbotron, MDBBtn, MDBContainer, MDBRow, MDBCol, MDBCardTitle, MDBIcon } from "mdbreact";

const JumbotronPage = () => {
  return (
    <MDBContainer className="col-auto p-0">
      <MDBRow className="">
        <MDBCol>
          <MDBJumbotron style={{ padding: 0 }}>
            <MDBCol className="text-white text-dark text-right px-4 " style={{ backgroundImage: `url(./banner.jpg)`}}>
              <MDBCol className="py-5">
              <MDBCardTitle className="h4-responsive text-dark text-right pt-3 m-5 font-bold">________Sale Off 40% </MDBCardTitle>
                <MDBCardTitle className="h1-responsive pt-3 m-5 text-dark font-bold">Summer Offer <br/> 2020 Collection </MDBCardTitle>
                <p className="mx-5 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                </p>
                <MDBBtn outline color="white btn-black" className="mb-5 m-5"><MDBIcon icon="clone" className="mr-2"></MDBIcon> View project</MDBBtn>
              </MDBCol>
            </MDBCol>
          </MDBJumbotron>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  )
}

export default JumbotronPage;