import React from 'react';
//import logo from './logo.svg';
import './App.css';
import NavbarPage from './homepage/NavbarPage';
import JumbotronPage from './homepage/JumbotronPage';
import Card from './homepage/Card';
import Feature from './homepage/Feature';
import Shopbycategory from './homepage/Shopbycategory';
import FooterPage from './homepage/FooterPage';
import Productpage from './homepage/Productpage';



function App() {
  return (
    <div>
        <NavbarPage/>
        <JumbotronPage/>
        <Card/>
        <Feature/>
        <Shopbycategory/>
        <Productpage/>
        <FooterPage/>
     </div>

  );
}


export default App;
