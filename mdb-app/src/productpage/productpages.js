import React from 'react';
import { MDBBtn, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBRow, MDBCol, MDBIcon } from
'mdbreact';

const Productpages = () => {
  return (
    <MDBRow className="justify-content-center m-5">
    <MDBCol md='12' lg='12' sm='12' className="m-5">
            <h2>Our Letest Product</h2>
        </MDBCol>
      <MDBCol md="3">
        <MDBCard cascade>
          <MDBCardImage
            cascade
            className='img-fluid'
            overlay="white-light"
            hover
            src='./product_01.jpg'
          />
          <MDBBtn
            floating
            tag='a'
            className=''
            action
          >
            <MDBIcon icon='shopping-cart' className=" falex fa-2x lighten-3"/>
          </MDBBtn>
          <MDBCardBody cascade>
            <MDBCardTitle>Red Strip Dress</MDBCardTitle>
            <hr/>
            <MDBCardText>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </MDBCardText>
          </MDBCardBody>
          <div className='rounded-bottom mdb-color lighten-0 text-center pt-3'>
            <ul className='list-unstyled list-inline font-small'>

              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon far icon='comments' className='mr-1' />
                  12
                </a>
              </li>
              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='facebook-f' className='mr-1' />
                  21
                </a>
              </li>
              <li className='list-inline-item'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='twitter' className='mr-1' />5
                </a>
              </li>
            </ul>
          </div>
        </MDBCard>
      </MDBCol>
      <MDBCol md="3">
        <MDBCard cascade>
          <MDBCardImage
            cascade
            className='img-fluid'
            overlay="white-light"
            hover
            src='./product_02.jpg'
          />
          <MDBBtn
            floating
            tag='a'
            className=''
            action
          >
            <MDBIcon icon='shopping-cart' className=" falex fa-2x lighten-3"/>
          </MDBBtn>
          <MDBCardBody cascade>
            <MDBCardTitle>Red Strip Dress</MDBCardTitle>
            <hr/>
            <MDBCardText>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </MDBCardText>
          </MDBCardBody>
          <div className='rounded-bottom mdb-color lighten-0 text-center pt-3'>
            <ul className='list-unstyled list-inline font-small'>

              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon far icon='comments' className='mr-1' />
                  12
                </a>
              </li>
              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='facebook-f' className='mr-1' />
                  21
                </a>
              </li>
              <li className='list-inline-item'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='twitter' className='mr-1' />5
                </a>
              </li>
            </ul>
          </div>
        </MDBCard>
      </MDBCol>
      <MDBCol md="3">
        <MDBCard cascade>
          <MDBCardImage
            cascade
            className='img-fluid'
            overlay="white-light"
            hover
            src='./product_03.jpg'
          />
          <MDBBtn
            floating
            tag='a'
            className=''
            action
          >
            <MDBIcon icon='shopping-cart' className=" falex fa-2x lighten-3"/>
          </MDBBtn>
          <MDBCardBody cascade>
            <MDBCardTitle>Red Strip Dress</MDBCardTitle>
            <hr/>
            <MDBCardText>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </MDBCardText>
          </MDBCardBody>
          <div className='rounded-bottom mdb-color lighten-0 text-center pt-3'>
            <ul className='list-unstyled list-inline font-small'>

              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon far icon='comments' className='mr-1' />
                  12
                </a>
              </li>
              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='facebook-f' className='mr-1' />
                  21
                </a>
              </li>
              <li className='list-inline-item'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='twitter' className='mr-1' />5
                </a>
              </li>
            </ul>
          </div>
        </MDBCard>
      </MDBCol>
      <MDBCol md="3">
        <MDBCard cascade>
          <MDBCardImage
            cascade
            className='img-fluid'
            overlay="white-light"
            hover
            src='./product_04.jpg'
          />
          <MDBBtn
            floating
            tag='a'
            className=''
            action
          >
            <MDBIcon icon='shopping-cart' className=" falex fa-2x lighten-3"/>
          </MDBBtn>
          <MDBCardBody cascade>
            <MDBCardTitle>Red Strip Dress</MDBCardTitle>
            <hr/>
            <MDBCardText>
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </MDBCardText>
          </MDBCardBody>
          <div className='rounded-bottom mdb-color lighten-0 text-center pt-3'>
            <ul className='list-unstyled list-inline font-small'>

              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon far icon='comments' className='mr-1' />
                  12
                </a>
              </li>
              <li className='list-inline-item pr-2'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='facebook-f' className='mr-1' />
                  21
                </a>
              </li>
              <li className='list-inline-item'>
                <a href='#!' className='white-text'>
                  <MDBIcon fab icon='twitter' className='mr-1' />5
                </a>
              </li>
            </ul>
          </div>
        </MDBCard>
      </MDBCol>
    </MDBRow>
  )
}

export default Productpages;