import React from 'react';
//import logo from './logo.svg';
import './App.css';
import NavbarPage from './productpage/NavbarPage';
import Productpages from './productpage/Productpages';
import FooterPage from './productpage/FooterPage';

function Product_app() {
  return (
    <div>
        <NavbarPage/>
        <Productpages/>
        <FooterPage/>
    </div>
  );
}

export default Product_app;
