import React from 'react';
//import logo from './logo.svg';
import './App.css';
import NavbarPage from './sign_up/NavbarPage';
import SigninPage from './sign_in/SigninPage';
import FooterPage from './sign_up/FooterPage';

function Sign_in_app() {
  return (
    <div>
        <NavbarPage/>
        <SigninPage/>
        <FooterPage/>
    </div>
  );
}
export default Sign_in_app;

