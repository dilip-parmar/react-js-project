import React from 'react';
//import logo from './logo.svg';
import './App.css';
import NavbarPage from './sign_up/NavbarPage';
import Sign_up from './sign_up/Sign_up';
import FooterPage from './sign_up/FooterPage';

function Sign_up_app() {
  return (
    <div>
        <NavbarPage/>
        <Sign_up/>
        <FooterPage/>
    </div>
  );
}

export default Sign_up_app;

